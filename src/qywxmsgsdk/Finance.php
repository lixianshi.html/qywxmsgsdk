<?php

namespace qywxmsgsdk;

use qywxmsgsdk\Contract\ProviderInterface;
use qywxmsgsdk\Provider\PHPExtProvider;

/**
 * Finance.
 * @method array getConfig(int $id)  获取微信配置
 * @method string getChatData(int $seq, int $limit)  获取会话记录数据(加密)
 * @method string decryptData(string $randomKey, string $encryptStr)  解密数据
 * @method \SplFileInfo getMediaData(string $sdkFileId, string $ext)  获取媒体资源
 * @method array getDecryptChatData(int $seq, int $limit)  获取会话记录数据(解密)
 */
class Finance
{
    /**
     * @var array
     */
    protected $config;

    /**
     * 初始化配置获取实例
     * @param array $wxConfig
     * @return ProviderInterface
     */
    public static function init(array $wxConfig = []): ProviderInterface
    {
        $provider = new PHPExtProvider;
        $provider->setConfig($wxConfig);
        return $provider;//返回一个实例
    }
}
