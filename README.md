# qywxmsgsdk

#### 介绍
企业微信会话存档PHP版sdk-linux环境

#### 软件架构
使用PHP跨语言FFI扩展。


#### 安装教程

1.  composer require lxs/qywxmsgsdk

#### 使用说明

1.  PHP >= 7.4
2.  需要有PHP-FFI插件

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
